import React, {Component}from "react";
import FindImage from './components/searchImages/findImage';
import DisplayImage from './components/displayImages/display';

class App extends Component {
  state = {
    result: []
  }
  handlePassData = (data) =>{
    this.setState({
      result: data
    })
  }
  
   render() {
    return (
      <div className="App">
       <FindImage handlePassData={this.handlePassData}/>
       <DisplayImage dataToRender={this.state.result}/>
      </div>
    );
  }
}

export default App;
