import React, { Component } from 'react'
import axios from 'axios';
import './findImage.css'; //css for the searchinput and button
class findImage extends Component {
    state = {
       photo: '',
       clientId: '_qBML_s212LqjtM5fvTdktbFBbemR9GBT7mqzF2Ext4',
       
    }

    handleChange = (e) => {
        this.setState({
            photo: e.target.value
        })
    }

    handleSubmit= (e) => {
        const {handlePassData} = this.props;
        const url ="https://api.unsplash.com/search/photos?query="+this.state.photo+"&client_id="+this.state.clientId;
        //call to get the image
        axios({
            method: "GET",
            url,
        })
        .then((res)=> {
            //pass the search data to another component to display
            handlePassData(res.data.results);
        })
    }
    
    render() {
        return(
            <div>
                <h1>Photo Search</h1>
                <input className="inputSearch" onChange={this.handleChange} type="text" name="photo" placeholder='Search for Photos....'/>
                
                <button className="buttonSearch" onClick={this.handleSubmit} type="submit">Search</button>
                
            </div>
        )
    }
}

export default findImage;