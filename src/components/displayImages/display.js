import React, { Component } from 'react'
import { ImageList, ImageListItem } from '@mui/material';

class displayImages extends Component {
  
    render() {
        const {dataToRender} = this.props;
        return(
            <div>
                <ImageList
                    variant="quilted"
                    cols={4}
                    rowHeight={'auto'}
                    >
                    {dataToRender.map((item) => (
                        <ImageListItem key={item.img} cols={item.cols || 1} rows={item.rows || 1}>
                        <img
                                src={item.urls.small} alt={item.alt_description}
                            loading="lazy"
                        />
                        </ImageListItem>
                    ))}
                    </ImageList>
            </div>
        )
    }
}

export default displayImages;